<?php
/*
  Plugin Name: CM Ad Changer - Client Pro
  Plugin URI: http://ad-changer.cminds.com/
  Description: Ad Changer Pro Client. Establish a connection with CM Ad Changer - Server Pro and retrieve campaigns and banners
  Author: CreativeMindsSolutions
  Version: 1.6.8
 */

/**
 * CM Ad Changer
 *
 * @author CreativeMinds (http://ad-changer.cminds.com)
 * @copyright Copyright (c) 2013, CreativeMinds
 */
define('AC_PLUGIN_PATH', WP_PLUGIN_DIR . '/' . basename(dirname(__FILE__)));
define('AC_PLUGIN_FILE', __FILE__);

require_once AC_PLUGIN_PATH . '/config.php';
require_once AC_PLUGIN_PATH . '/functions.php';
require_once AC_PLUGIN_PATH . '/licensing_api.php';
require_once AC_PLUGIN_PATH . '/classes/ac_client.php';

$licensingApi = new CMAC_Cminds_Licensing_API('CM Ad Changer Pro', 'cm_acc_admin', 'Ad Changer', AC_PLUGIN_FILE, array('release-notes' => 'http://ad-changer.cminds.com/release-notes/'), '', array('CM Ad Changer Pro', 'CM Ad Changer Pro Special'));
$isLicenseOk = $licensingApi->isLicenseOk();

if( !class_exists('Image') )
{
    require_once AC_PLUGIN_PATH . '/libs/image.php';
}
/*
 * Single banner widget
 */
if( !class_exists('CMACWidget') )
{
    require_once AC_PLUGIN_PATH . '/widget.php';
}
/*
 * Banner group widget
 */
if( !class_exists('CMACGWidget') )
{
    require_once AC_PLUGIN_PATH . '/groupswidget.php';
}

register_activation_hook(__FILE__, 'ads_cc_activate');
register_deactivation_hook(__FILE__, 'ads_cc_deactivate');

$errors = array();
$success = "";
if( $isLicenseOk && !shortcode_exists('cm_ad_changer') ){
    add_action('admin_init', 'cmac_init');
}

function cmac_init() {
    wp_enqueue_script('jquery-ouibounce', plugins_url('assets/js/ouibounce.js', __FILE__), array('jquery'));
    wp_enqueue_script('jquery-flyingBottom', plugins_url('assets/js/flyingBottom.js', __FILE__), array('jquery'));
    wp_enqueue_style('cm-ad-ouibounce', plugins_url('assets/css/ouibounce.css', __FILE__));
    add_shortcode('cm_ad_changer', array('AC_Client', 'banners'));
}
add_action('admin_notices', 'cmac_display_notice');

function cmac_display_notice()
{
    $serverActive = get_option('acc_active', null);
    if( !empty($_GET['page']) && $_GET['page'] == 'cm_acc_admin' && !empty($_POST) )
    {
        $serverActive = $_POST['cm_acc_admin'];
    }

    if( !$serverActive )
    {
        $message = sprintf(__('%s has detected that the Client is not Active.'), 'CM Ad Changer Pro');
        cminds_show_message($message, true);
    }
}

function ads_cc_init()
{
    global $errors, $success;

    wp_register_style('ACCStylesheet', plugins_url('assets/css/style.css', __FILE__));
    wp_register_style('jqueryUIStylesheet', plugins_url('assets/css/jquery-ui/smoothness/jquery-ui-1.10.3.custom.css', __FILE__));
    wp_register_style('blueimp-gallery.min', plugins_url('assets/js/blueimp-jQuery-Image-Gallery/css/blueimp-gallery.min.css', __FILE__));

    if( isset($_POST['action']) && $_POST['action'] == 'acc_settings' )
    {
        if( !isset($_POST['general_settings_noncename']) || !wp_verify_nonce($_POST['general_settings_noncename'], 'CM_ADCHANGER_CLIENT_GENERAL_SETTINGS') ) {
            exit('Bad Request!');
        };
        if( !preg_match('/^((http|https|ftp):\/\/)?[\.a-zA-Z0-9_\-]{2,100}\.[a-zA-Z0-9]{2,5}\/?[a-zA-Z0-9_\-\/]{0,100}:?\d{0,10}$/i', $_POST['acc_server_domain'], $matches) )
        {
            $errors[] = 'Please specify valid Server Domain Name';
        }

        if( isset($_POST['acc_class_name']) && strlen($_POST['acc_class_name']) > 50 )
        {
            $errors[] = 'Class Name is too long';
        }

        if( isset($_POST['acc_custom_css']) && strlen($_POST['acc_custom_css']) > 500 )
        {
            $errors[] = 'Custom CSS is too long';
        }

        if( !empty($errors) )
        {
            return;
        }

        if( isset($_POST['acc_active']) )
        {
            update_option('acc_active', 1);
        }
        else
        {
            update_option('acc_active', 0);
        }
        
        if( isset($_POST['acs_disable_client_history_table']) ){
            update_option('acs_disable_client_history_table', 1);
        }
        else{
            update_option('acs_disable_client_history_table', 0);
        }

        if( isset($_POST['acc_inject_scripts']) )
        {
            update_option('acc_inject_scripts', 1);
        }
        else
        {
            update_option('acc_inject_scripts', 0);
        }

        if( isset($_POST['acc_custom_css']) )
        {
            update_option('acc_custom_css', $_POST['acc_custom_css']);
        }
        else
        {
            update_option('acc_custom_css', '');
        }

        if( isset($_POST['acc_server_domain']) )
        {
            update_option('acc_server_domain', $_POST['acc_server_domain']);
        }

        if( isset($_POST['acc_use_banner_variations']) )
        {
            update_option('acc_use_banner_variations', $_POST['acc_use_banner_variations']);
        }

        if( isset($_POST['acc_banner_area']) )
        {
            update_option('acc_banner_area', $_POST['acc_banner_area']);
        }

        if( isset($_POST['acc_resize_banner']) )
        {
            update_option('acc_resize_banner', '1');
        }
        else
        {
            update_option('acc_resize_banner', '0');
        }

        // Updating slideshow start
        if( isset($_POST['acc_slideshow_effect']) )
        {
            update_option('acc_slideshow_effect', $_POST['acc_slideshow_effect']);
        }

        if( isset($_POST['acc_slideshow_interval']) )
        {
            update_option('acc_slideshow_interval', $_POST['acc_slideshow_interval']);
        }

        if( isset($_POST['acc_slideshow_transition_time']) )
        {
            update_option('acc_slideshow_transition_time', $_POST['acc_slideshow_transition_time']);
        }

        // Updating slideshow end
        if( isset($_POST['acc_campaign_id']) )
        {
            update_option('acc_campaign_id', $_POST['acc_campaign_id']);
        }

        // Scripts in footer
        if( isset($_POST['acc_script_in_footer']) ){
            update_option('acc_script_in_footer', $_POST['acc_script_in_footer']);
        }else{
            update_option('acc_script_in_footer', 0);
        }

        $success = 'Settings Stored!';
    }
}

add_action('admin_init', 'ads_cc_init');

function cm_acc_menu()
{
    global $submenu;
    $current_user = wp_get_current_user();
    if( !user_can($current_user, 'manage_options') )
    {
        return;
    }

    $page = add_menu_page('CM Ad Changer - Client Pro', 'CM Ad Changer', 'manage_options', 'cm_acc_admin', 'cm_acc_load_page', plugin_dir_url(__FILE__) . '/assets/images/cm-ad-changer-icon.png');
    $testing_subpage = add_submenu_page('cm_acc_admin', 'Testing', 'Testing', 'manage_options', 'cm_acc_testing', 'cm_acc_load_page');
    $check_connection_subpage = add_submenu_page('cm_acc_admin', 'Check Server Connection', 'Check Server Connection', 'manage_options', 'cm_acc_check_connection', 'cm_acc_load_page');
    $about_subpage = add_submenu_page('cm_acc_admin', 'About', 'About', 'manage_options', 'cm_acc_about', 'cm_acc_load_page');

    $submenu['cm_acc_admin'][500] = array('User Guide', 'edit_posts', 'http://ad-changer.cminds.com/ad-changer-user-guide/');

    add_action('admin_print_styles-' . $page, 'cm_acc_head');
    add_action('admin_print_styles-' . $testing_subpage, 'cm_acc_head');
    add_action('admin_print_styles-' . $check_connection_subpage, 'cm_acc_head');
    add_action('admin_print_styles-' . $about_subpage, 'cm_acc_head');

    $submenu['cm_acc_admin'][0][0] = 'Settings';
}

add_action('admin_menu', 'cm_acc_menu');

function cm_acc_head($hook)
{
    if( $_GET['page'] == 'cm_acc_testing' )
    {
        if( !defined('CMAC_HEAD_ENQUEUED') )
        {
            define('CMAC_HEAD_ENQUEUED', true);
        }
        wp_enqueue_script('tcycle', plugins_url('assets/js/jquery.tcycle.js', __FILE__), array('jquery'), '1.0.0', false);
        wp_enqueue_script('jquery-ouibounce', plugins_url('assets/js/ouibounce.js', __FILE__), array('jquery'));
        wp_enqueue_style('cm-ad-ouibounce', plugins_url('assets/css/ouibounce.css', __FILE__));
    }

    $int_version = (int) str_replace('.', '', get_bloginfo('version'));
    if( $int_version < 100 )
    {
        $int_version *= 10; // will be 340 or 341 or 350 etc
    }
    wp_enqueue_style('ACCStylesheet');
    wp_enqueue_style('jqueryUIStylesheet');

    if( $int_version > 320 )
    {
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-tooltip');
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('jquery-ui-widget');
        wp_enqueue_script('jqueryUIPosition');
    }
    else
    {
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-tabs');
        wp_enqueue_script('jquery-ui-widget');
        wp_enqueue_script('jqueryUIPosition');
    }

    if( $int_version >= 350 )
    {
        wp_enqueue_script('jquery-ui-tooltip');
    }

    if( $int_version < 350 )
    {
        wp_enqueue_script('jQueryMissingUI', plugins_url('assets/js/jquery-ui/missing_ui.js', __FILE__), array(), '1.0.0', false);
    }
}

/**
 * Pages dispatcher
 */
function cm_acc_load_page()
{
    global $label_descriptions;
    $plugin_data = get_plugin_data(__FILE__);

    if( isset($_GET['page']) )
    {
        if( !cm_acc_check_server_activated() )
        {
            $errors = array('CM Ad Changer Server is activated. Please either deactivate it or deactivate Ad Changer Client plugin, Ad Changer Server has it\'s inbuilt client');
            require_once AC_PLUGIN_PATH . '/views/error.php';
            return;
        }

        switch($_GET['page'])
        {
            case 'cm_acc_admin':
                global $errors, $success, $submenu;
                $fields_data = array();

                $fields_data['acc_active'] = get_option('acc_active', '1');
                $fields_data['acs_disable_client_history_table'] = get_option('acs_disable_client_history_table', false);
                $fields_data['acc_server_domain'] = get_option('acc_server_domain', '');
                $fields_data['acc_inject_scripts'] = get_option('acc_inject_scripts', '0');
                $fields_data['acc_div_wrapper'] = get_option('acc_div_wrapper', '0');
                $fields_data['acc_class_name'] = get_option('acc_class_name', '');
                $fields_data['acc_use_banner_variations'] = get_option('acc_use_banner_variations', '1');
                $fields_data['acc_banner_area'] = get_option('acc_banner_area', 'screen');
                $fields_data['acc_resize_banner'] = get_option('acc_resize_banner', '1');

                $fields_data['acc_slideshow_effect'] = get_option('acc_slideshow_effect', 'fade');
                $fields_data['acc_slideshow_interval'] = get_option('acc_slideshow_interval', '5000');
                $fields_data['acc_slideshow_transition_time'] = get_option('acc_slideshow_transition_time', '400');

                $fields_data['acc_custom_css'] = get_option('acc_custom_css', '');
                $fields_data['acc_script_in_footer'] = get_option('acc_script_in_footer', '0');

                if( isset($_POST['action']) && $_POST['action'] == 'acc_settings' )
                {
                    $fields_data = array_merge($fields_data, $_POST);
                }

                require_once AC_PLUGIN_PATH . '/views/settings.php';
                break;

            case 'cm_acc_testing':
                if( !empty($_POST) ) $fields_data['acc_campaign_id'] = $_POST['acc_campaign_id'];
                require_once AC_PLUGIN_PATH . '/views/testing.php';
                break;
            case 'cm_acc_check_connection':
                if( !empty($_POST['check-connection']) ){ 
                    $fields_data['check-connection'] = $_POST['check-connection'];
                }
                require_once AC_PLUGIN_PATH . '/views/check_connection.php';
                break;
            case 'cm_acc_about':
                require_once AC_PLUGIN_PATH . '/views/about.php';
                break;
        }
    }
}

function cm_acc_top_menu()
{
    global $submenu;
    $current_slug = $_GET['page'];
    ?>

    <style type="text/css">
        .subsubsub li+li:before {content:'| ';}
    </style>
    <ul class="subsubsub">
        <?php foreach($submenu['cm_acc_admin'] as $menu): ?>
            <?php
            $isExternalPage = strpos($menu[2], 'http') !== FALSE || strpos($menu[2], '.php') !== FALSE;
            $targetUrl = $isExternalPage ? $menu[2] : 'admin.php?page=' . $menu[2];
            $newTab = $isExternalPage ? 'target="_blank"' : '';
            ?>
            <li><a href="<?php echo $targetUrl ?>" <?php echo ($_GET['page'] == $targetUrl) ? 'class="current"' : ''; ?> <?php echo $newTab; ?>><?php echo $menu[0]; ?></a></li>
    <?php endforeach; ?>
    </ul>
    <?php
}
