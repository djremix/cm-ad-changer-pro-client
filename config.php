<?php
/**
 * CM Ad Changer
 *
 * @author CreativeMinds (http://ad-changer.cminds.com)
 * @copyright Copyright (c) 2013, CreativeMinds
 */
if(!isset($table_prefix)){
    $table_prefix = '';
}
define('CMAC_DEBUG', '0');
define('ACC_UPLOAD_PATH', 'acc_uploads/');
define('PENDING_EVENTS_TABLE', $table_prefix . 'cm_pending_events');

$label_descriptions = array(
    'acc_active'                    => 'If you turn off the plugin it will not communicate with &quot;CM Ad Changer - Pro Server&quot; (banners will not be displayed)',
    'acc_server_domain'             => 'Domain address of the Wordpress installation running &quot;CM Ad Changer - Pro Server&quot;. (should include the protocol: http:// or https:// )',
    'acc_inject_scripts'            => 'Injecting scripts into all pages is needed in rare cases when campaign function or shortcode is called from an external plugin. This means that every page uploaded from your site will have the js/css code associated with Ad Changer.',
    'acc_script_in_footer'          => 'Inject scripts in footer. Warning: theme must use wp_footer() function for this to work!',
    'acc_div_wrapper'               => 'Div Wrapper(client side) - Will add div around banner',
    'acc_class_name'                => 'Class Name - Will set the class name for div',
    'acc_slideshow_effect'          => 'Which effect should be used to rotate banners',
    'acc_slideshow_interval'        => 'The amount of time each banner is displayed (milliseconds)',
    'acc_slideshow_transition_time' => 'The amount of time each transition takes (milliseconds)',
    'acc_use_banner_variations'     => 'If set, banner variations will be used when screen or container size is smaller than served banner.  ',
    'acc_banner_area'               => 'Choose what the variation size will be based on. "Container" means the size of the containing element while "Screen" is the detected screen/device size.',
    'acc_resize_banner'             => 'In case no banner variations exist, allow to resize the existing banner to fit the screen/container size accordingly.',
    'acc_custom_css'                => 'Custom CSS style: Custom CSS will be injected into body before banner is shown and only on post or pages where campaign is active. Example: #featured.has-badge {margin-bottom: 85px;}',
    'acs_disable_client_history_table' => 'Select this option to turn off AdChanger functionality of tracking banner clicks and impressions. Selecting this option may speed up the site since AdChanger will not be sending clicks requests.');
