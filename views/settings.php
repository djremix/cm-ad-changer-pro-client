<?php
/**
 * CM Ad Changer
 *
 * @author CreativeMinds (http://ad-changer.cminds.com)
 * @copyright Copyright (c) 2013, CreativeMinds
 */
?>

<div class="wrap ad_changer cm-ads-changer-client">
    <h2><?php echo $plugin_data['Name']; ?></h2>
    <?php
    cm_acc_top_menu();
    if( isset($errors) && !empty($errors) )
    {
        ?>
        <ul class="acc_error cmac-clear">
            <?php
            foreach($errors as $error) echo '<li>' . $error . '</li>';
            ?>
        </ul>
        <?php
    }
    if( isset($success) && !empty($success) )
    {
        echo '<div class="acc_success cmac-clear">' . $success . '</div>';
    }
    ?>
    <div class="acc-shortcode-reference cmac-clear cminds_settings_description">
        <p>To insert the ads container into a page or post use following shortcode: [cm_ad_changer]. <br/>
            To use it outsite of the content use the following code: <code>&lt;?php echo do_shortcode('[cm_ad_changer]'); ?&gt;</code></p>
        <p>Here is the list of parameters: <a href="javascript:void(0)" onclick="jQuery(this).parent().next().slideToggle()">Show/Hide</a></p>
        <ul style="list-style-type: disc; margin-left: 20px;">
            <li>
                <strong>campaign_id</strong> - ID of a campaign (required*)
            </li>
            <li>
                <strong>group_id</strong> - ID of a campaign group (required*)
            </li>
            <li>
                <strong>linked_banner</strong> - Banner is a linked image or just image. Can be 1 or 0 (default: 1)
            </li>
            <li>
                <strong>debug</strong> - Show the debug info. Can be 1 or 0 (default: 0)
            </li>
            <li>
                <strong>wrapper</strong> - Wrapper On or Off. Wraps banner with  a div tag. Can be 1 or 0 (default: 0)
            </li>
            <li>
                <strong>class</strong> - Banner (div) class name
            </li>
            <li>
                <strong>no_responsive</strong> - Banner not responsive. Can be 1 or 0 (default: 0)
            </li>
            <li>
                <strong>custom_css</strong> - The CSS code which would only be outputted if the banner is shown. (default: empty)
            </li>
            <li>
                <strong>allow_inject_js</strong> - Whether to allow server to inject JS or not. Can be 1 or 0 (default: 0)
            </li>
            <li>
                <strong>allow_inject_html</strong> - Whether to allow server to send the HTML Ads or not. Can be 1 or 0 (default: 0)
            </li>
            <li>
                <strong>width</strong> - Width of the banner image (default: auto)
            </li>
            <li>
                <strong>height</strong> - Height of the banner image (default: auto)
            </li>
            <li style="list-style: none">
                <div class="hint">
                    <i>* - You have to provide either a Group ID or a Campaign ID</i>
                </div>
            </li>
        </ul>
    </div>

    <?php
    include plugin_dir_path(__FILE__) . '/call_to_action.phtml';
    ?>

    <br/>
    <div class="cmac-clear"></div>

    <div class="acc-edit-form cmac-clear">
        <form id="acc_settings_form" method="post">
            <input type="hidden" name="action" value="acc_settings" />
            <div id="settings_fields" class="cmac-clear">
                <ul>
                    <li><a href="#general_settings_fields">General Settings</a></li>
                    <li><a href="#custom_css_fields">Custom CSS</a></li>
                    <li><a href="#banner_rotation_fields">Rotated Banners</a></li>
                    <li><a href="#responsive_settings">Responsive Settings</a></li>
                    <li><a href="#server-info">Server Information</a></li>
                </ul>
                <table cellspacing=3 cellpadding=0 border=0 id="general_settings_fields">
                    <tr>
                        <td>
                            <label class="acc-form-label" for="acc_active" >Client Active <div class="field_help" title="<?php echo $label_descriptions['acc_active'] ?>"></div></label>
                        </td>
                        <td>
                            <input type="checkbox" name="acc_active" id="acc_active" value="1" <?php checked('1', $fields_data['acc_active']); ?> />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="acc-form-label" for="acs_disable_client_history_table" >Disable sending history data  <div class="field_help" title="<?php echo $label_descriptions['acs_disable_client_history_table'] ?>"></div></label>
                        </td>
                        <td>
                            <input type="checkbox" name="acs_disable_client_history_table" id="acs_disable_client_history_table" value="1" <?php checked('1', $fields_data['acs_disable_client_history_table']); ?> />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="acc-form-label" for="acc_server_domain" >Ad Server domain <span class="required">*</span> </label>
                            <div class="field_help" title="<?php echo $label_descriptions['acc_server_domain'] ?>"></div>
                        </td>
                        <td>
                            <input type="text" name="acc_server_domain" id="acc_server_domain" value="<?php echo $fields_data['acc_server_domain'] ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="acc-form-label" for="acc_inject_scripts" >Inject JS libraries on ALL pages</label>
                            <div class="field_help" title="<?php echo $label_descriptions['acc_inject_scripts'] ?>"></div>
                        </td>
                        <td>
                            <input type="checkbox" name="acc_inject_scripts" id="acc_inject_scripts"  value="1" <?php checked('1', $fields_data['acc_inject_scripts']); ?> />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="acc-form-label" for="acc_inject_scripts" >Inject JS files in footer</label>
                            <div class="field_help" title="<?php echo $label_descriptions['acc_script_in_footer'] ?>"></div>
                        </td>
                        <td>
                            <input type="checkbox" name="acc_script_in_footer" id="acc_script_in_footer"  value="1" <?php checked('1', $fields_data['acc_script_in_footer']); ?> />
                        </td>
                    </tr>
                </table>
                <table cellspacing=3 cellpadding=0 border=0 id="custom_css_fields">
                    <tr>
                        <td style="width:150px !important">
                            <label class="acc-form-label" for="acc_custom_css" >Custom CSS </label><div class="field_help" title="<?php echo $label_descriptions['acc_custom_css'] ?>"></div>
                        </td>
                        <td>
                            <textarea id="acc_custom_css" name="acc_custom_css" rows=7 cols=60 value="<?php echo stripslashes($fields_data['acc_custom_css']) ?>"><?php echo stripslashes($fields_data['acc_custom_css']) ?></textarea>
                        </td>
                    </tr>
                </table>
                <table id="banner_rotation_fields">
                    <tr>
                        <td width="48%">
                            <label class="acc-form-label" for="acc_slideshow_effect" >Rotated Banner switch effect </label>
                            <div class="field_help" title="<?php echo $label_descriptions['acc_slideshow_effect'] ?>"></div>
                        </td>
                        <td>
                            <select id="acc_slideshow_effect" name="acc_slideshow_effect">
                                <option value="fade" <?php echo!isset($fields_data['acc_slideshow_effect']) || $fields_data['acc_slideshow_effect'] == 'fade' ? 'selected=selected' : '' ?>>Fade</option>
                                <option value="slide" <?php echo isset($fields_data['acc_slideshow_effect']) && $fields_data['acc_slideshow_effect'] == 'slide' ? 'selected=selected' : '' ?>>Slide</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="acc-form-label" for="acc_slideshow_interval" >Rotated Banner switch interval </label>
                            <div class="field_help" title="<?php echo $label_descriptions['acc_slideshow_interval'] ?>"></div>
                        </td>
                        <td>
                            <input type="text" name="acc_slideshow_interval" id="acc_slideshow_interval" value="<?php echo $fields_data['acc_slideshow_interval'] ?>" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="acc-form-label" for="acc_slideshow_transition_time" >Rotated Banner Transition time </label>
                            <div class="field_help" title="<?php echo $label_descriptions['acc_slideshow_transition_time'] ?>"></div>
                        </td>
                        <td>
                            <input type="text" name="acc_slideshow_transition_time" id="acc_slideshow_transition_time" value="<?php echo $fields_data['acc_slideshow_transition_time'] ?>" />
                        </td>
                    </tr>
                </table>
                <table id="responsive_settings">
                    <tr><td colspan=2><p>Using banner variations means it will take more time to show ad on client side since it is called only after calculating possible size</p><br></td></tr>
                    <tr>
                        <td>
                            <label class="acc-form-label" for="acc_use_banner_variations" >Use Banner Variations </label><div class="field_help" title="<?php echo $label_descriptions['acc_use_banner_variations'] ?>"></div>
                        </td>
                        <td>
                            <input type="radio" value="1" name="acc_use_banner_variations" id="acc_use_banner_variations" <?php echo!isset($fields_data['acc_use_banner_variations']) || $fields_data['acc_use_banner_variations'] == '1' ? 'checked=checked' : '' ?> />&nbsp;<label for="acc_use_banner_variations" >Yes</label><br/>
                            <input type="radio" value="0" name="acc_use_banner_variations" id="acc_not_use_banner_variations" <?php echo isset($fields_data['acc_use_banner_variations']) && $fields_data['acc_use_banner_variations'] == '0' ? 'checked=checked' : '' ?> />&nbsp;<label for="acc_not_use_banner_variations" >No</label>
                            <div id="variations_settings">
                                <label class="acc-form-label" >Choose variation based on width of </label><div class="field_help" title="<?php echo $label_descriptions['acc_banner_area'] ?>"></div><br/>
                                <input type="radio" value="screen" name="acc_banner_area" id="screen" <?php echo!isset($fields_data['acc_banner_area']) || $fields_data['acc_banner_area'] == 'screen' ? 'checked=checked' : '' ?> />&nbsp;<label for="screen" >Screen</label><br/>
                                <input type="radio" value="container" name="acc_banner_area" id="container" <?php echo isset($fields_data['acc_banner_area']) && $fields_data['acc_banner_area'] == 'container' ? 'checked=checked' : '' ?> />&nbsp;<label for="container" >Container</label><br/><br/>
                                <label class="acc-form-label" >If no variations are available resize banner </label><div class="field_help" title="<?php echo $label_descriptions['acc_resize_banner'] ?>"></div>
                                <input type="checkbox" value="1" name="acc_resize_banner" id="screen" <?php echo isset($fields_data['acc_resize_banner']) && $fields_data['acc_resize_banner'] == '1' ? 'checked=checked' : '' ?> />
                            </div>
                        </td>
                    </tr>

                </table>

                <!-- Start Server information Module -->
                <div id="server-info">
                    <div class='block'>
                        <h3>Server Information</h3>
                        <?php
                        $safe_mode = ini_get('safe_mode') ? ini_get('safe_mode') : 'Off';
                        $upload_max = ini_get('upload_max_filesize') ? ini_get('upload_max_filesize') : 'N/A';
                        $post_max = ini_get('post_max_size') ? ini_get('post_max_size') : 'N/A';
                        $memory_limit = ini_get('memory_limit') ? ini_get('memory_limit') : 'N/A';
                        $cURL = function_exists('curl_version') ? 'On' : 'Off';

                        $php_info = parse_php_info();
                        ?>
                        <span class="description">This information is useful to check if plugin might have some incompabilities with you server.</span>
                        <table class="form-table server-info-table">
                            <tr>
                                <td>PHP Version</td>
                                <td><?php echo phpversion(); ?></td>
                                <td><?php if( version_compare(phpversion(), '5.3.0', '<') ): ?><strong>Recommended 5.3 or higher</strong><?php else: ?><span>OK</span><?php endif; ?></td>
                            </tr>
                            <tr>
                                <td>PHP Safe Mode</td>
                                <td><?php echo $safe_mode; ?></td>
                                <td><?php if( version_compare(phpversion(), '5.3.0', '<') ): ?><strong>Safe mode is deprecated</strong><?php else: ?><span>OK</span><?php endif; ?></td>
                            </tr>
                            <tr>
                                <td>PHP Memory Limit</td>
                                <td><?php echo $memory_limit; ?></td>
                                <td><?php if( cminds_units2bytes($memory_limit) < 1024 * 1024 * 128 ): ?>
                                        <strong>This value can be too lower to Wordpress with plugins work properly.</strong>
                                    <?php else: ?><span>OK</span><?php endif; ?></td>
                            </tr>
                            <tr>
                                <td>PHP cURL</td>
                                <td><?php echo $cURL; ?></td>
                                <td><?php if( $cURL == 'Off' ): ?>
                                        <strong>cURL library is required to use the Social Login.</strong>
                                    <?php else: ?><span>OK</span><?php endif; ?></td>
                            </tr>

                            <?php
                            if( isset($php_info['gd']) && is_array($php_info['gd']) )
                            {
                                foreach($php_info['gd'] as $key => $val)
                                {
                                    if( !preg_match('/(WBMP|XBM|Freetype|T1Lib)/i', $key) && $key != 'Directive' && $key != 'gd.jpeg_ignore_warning' )
                                    {
                                        echo '<tr>';
                                        echo '<td>' . $key . '</td>';
                                        if( stripos($key, 'support') === false )
                                        {
                                            echo '<td>' . $val . '</td>';
                                        }
                                        else
                                        {
                                            $val = true;
                                            echo '<td>enabled</td>';
                                        }

                                        echo '<td>';
                                        switch($key)
                                        {
                                            case 'GD Support':
                                                if( $val === true ) echo '<span>OK</span>';
                                                else echo '<strong>Required to display screenshots.</strong>';
                                                break;
                                        }
                                        echo '</td>';
                                        echo '</tr>';
                                    }
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
            <input type="submit" value="Store Settings" id="submit_button">
            <?php wp_nonce_field('CM_ADCHANGER_CLIENT_GENERAL_SETTINGS', 'general_settings_noncename'); ?>
        </form>
    </div>
    <!-- Test section -->
    <?php
    if( isset($fields_data['acc_campaign_id']) && $fields_data['acc_campaign_id'] > 0 )
    {
        echo '<h3>Test Campaign Output</h3>';
        $content_sortcode = '[cm_ad_changer campaign_id="' . $fields_data['acc_campaign_id'] . '" debug="1"]';
        echo '<br>Preforming the following shortcode:<br> <strong>' . $content_sortcode . '</strong><br><br>';
        echo do_shortcode($content_sortcode);
    }
    ?>
</div>


<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.field_help').tooltip({
            show: {
                effect: "slideDown",
                delay: 100
            },
            position: {
                my: "left top",
                at: "right top"
            }
        })

        jQuery('#settings_fields').tabs();

        jQuery('input[name="acc_use_banner_variations"]').click(function () {
            if (jQuery('#acc_use_banner_variations').attr('checked') == 'checked')
                jQuery('#variations_settings').show();
            else
                jQuery('#variations_settings').hide();
        })

        if (jQuery('input[name="acc_use_banner_variations"][checked="checked"]').val() == '1')
            jQuery('#variations_settings').show();
    });
</script>