<?php
/**
 * CM Ad Changer
 *
 * @author CreativeMinds (http://ad-changer.cminds.com)
 * @copyright Copyright (c) 2013, CreativeMinds
 */
?>

<div class="wrap cm-ads-changer-client ac_testing">
    <h2><?php echo $plugin_data['Name']; ?> : Check Server Connection</h2>
    <?php
    cm_acc_top_menu();
    ?>
    <div class="acc-shortcode-reference cmac-clear cminds_settings_description">
        <p>Here is the connection test know-how: <a href="javascript:void(0)" onclick="jQuery(this).parent().next().slideToggle()">Show/Hide</a></p>
        <ul style="list-style-type: disc; margin-left: 20px;">
            <li>
                <strong>Before performing the test make sure to fill in "Ad Server domain" field in the settings section.</strong>
            </li>
            <li>
                <strong>Click Test Connection Button</strong>
                <div class="hint">
                    <i> - plugin will perform test request to the server</i>
                </div>
            </li>
            <li>
                <strong>If test check is successful, green "Connection Successful!" message will appear below the "Test Connection" button</strong>
            </li>
            <li>
                <strong>If "Connection Successful!" message does not appear, that means there's a problem with connection between client and server side.</strong>
            </li>
            <li>
                <strong> Messages or other error codes returned from server will appear below "Test Connection" button.</strong>
                <div class="hint">
                    <i> - attaching screen shoot of message returned will help solve the possible error fix request by CreativeMinds support department</i>
                </div>
            </li>
        </ul>
    </div>
    <div class="acc-edit-form cmac-clear">
        <form id="acc_testing_form" method="post" action="">
            <!-- Test section -->
            <input type="hidden" name="check-connection" id="check-connection" value="1" />
            <input type="submit" value="Test Connection" id="submit_button" style="float: left !important;" />
        </form>
    </div>
    <div class="clear"></div>
    <?php
    if( isset($fields_data['check-connection']) &&  $fields_data['check-connection'] == 1 )
    {
        echo '<br><strong>Preforming Server Connection Test</strong><br>';
        $serverUrl = trailingslashit(get_option('acc_server_domain', ''));
        if (!empty($serverUrl)) {
            $responseString = AC_Client::curl_request($serverUrl . '?acs_action=check_server_connection');
        }else{
            viewMessage('Server URL is not set', 'error');
        }
        echo '<div><strong>Server Response:</strong><br/>';
        echo $responseString;
        echo '<div>';
    }
    ?>
</div>
</div>