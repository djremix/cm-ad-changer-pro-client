<?php
/**
 * CM Ad Changer
 *
 * @author CreativeMinds (http://ad-changer.cminds.com)
 * @copyright Copyright (c) 2013, CreativeMinds
 */
?>

<div class="wrap cm-ads-changer-client ac_testing">
    <h2><?php echo $plugin_data['Name']; ?> : Testing</h2>
    <?php
    cm_acc_top_menu();
    ?>

    <div class="acc-edit-form cmac-clear">
        <form id="acc_testing_form" method="post" style="width:300px ! important;">
            <!-- Test section -->
            <table cellspacing=3 cellpadding=0 border=0 id="testing_fields">
                <tr>
                    <td style="width:100px;">
                        <label class="acc-form-label" for="acc_campaign_id" >Ad Campaign ID: </label>
                    </td>
                    <td style="width: auto !important;">
                        <input type="text" name="acc_campaign_id" id="acc_campaign_id" style="width:30px" size="4" value="<?php echo isset($fields_data['acc_campaign_id']) ? $fields_data['acc_campaign_id'] : '' ?>" />
                    </td>
                </tr>

            </table>
            <input type="submit" value="Start Test" id="submit_button" style="float: left !important;">
        </form>
    </div>
    <?php
    if( isset($fields_data['acc_campaign_id']) &&  $fields_data['acc_campaign_id'] > 0 )
    {
        echo '<br/><br/><h3>Test Campaign Output</h3>';
        $content_sortcode = '[cm_ad_changer campaign_id="' . $fields_data['acc_campaign_id'] . '" debug="1"]';
        echo '<br>Preforming the following shortcode:<br> <strong>' . $content_sortcode . '</strong><br><br>';
        echo '<div><strong>Banners should appear below:</strong><br/>';
        echo do_shortcode($content_sortcode);
        echo '<div>';
    }
    ?>
</div>