<?php
/**
 * CM Ad Changer 
 *
 * @author CreativeMinds (http://ad-changer.cminds.com)
 * @copyright Copyright (c) 2013, CreativeMinds
 */
?>	
<div class="wrap cm-ads-changer-client">
	<h2><?php echo $plugin_data['Name']; ?></h2>
<?php	
	cm_acc_top_menu();	
	if(isset($errors)&&!empty($errors)){
?>
	<ul class="acc_error cmac-clear">
<?php
		foreach($errors as $error)
			echo '<li>'.$error.'</li>';
?>		
	</ul>
<?php		
	}
?>

	<div class="cm-acc-error cmac-clear">
	</div>
</div>