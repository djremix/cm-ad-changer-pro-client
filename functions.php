<?php

/**
 * CM Ad Changer
 *
 * @author CreativeMinds (http://ad-changer.cminds.com)
 * @copyright Copyright (c) 2013, CreativeMinds
 */
global $CMAdChangerClientCampaignCache, $CMAdChangerClientError;

/**
 * Plugin activation
 *
 */
function ac_activate($networkwide) {
    global $wpdb;

    if (function_exists('is_multisite') && is_multisite()) {
        /*
         * Check if it is a network activation - if so, run the activation function for each blog id
         */
        if ($networkwide) {
            /*
             * Get all blog ids
             */
            $blogids = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM {$wpdb->blogs}"));
            foreach ($blogids as $blog_id) {
                switch_to_blog($blog_id);
                cmacc__install();
            }
            restore_current_blog();
            return;
        }
    }

    cmacc__install();
}

function cmacc__install() {
    global $wpdb, $table_prefix; // have to use $table_prefix
//	$wpdb->query('DROP TABLE IF EXISTS '.$table_prefix.PENDING_EVENTS_TABLE);

//    $wpdb->query('CREATE TABLE IF NOT EXISTS ' . $table_prefix . PENDING_EVENTS_TABLE . ' (
//				  event_id int(11) NOT NULL AUTO_INCREMENT,
//				  campaign_id int(11) NOT NULL,
//				  event_type enum("click","impression") NOT NULL,
//				  server_url varchar(150) NOT NULL,
//				  attempts int(11) NOT NULL DEFAULT 1,
//				  reg_date timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
//				  PRIMARY KEY  (event_id)
//				) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;');

    $uploadDir = wp_upload_dir();
    $ac_uploadDir = $uploadDir['basedir'] . '/' . ACC_UPLOAD_PATH;
    if (!is_dir($ac_uploadDir)) {
        if (!wp_mkdir_p($ac_uploadDir)) {
            echo 'Error: Your WP uploads folder is not writable! The plugin requires a writable uploads folder in order to work.';
            exit;
        }
    }
}

/**
 * Returns the upload dir
 * @return string
 */
function cmacc_get_upload_dir() {
    static $cmacUpladDir = null;

    if (!$cmacUpladDir) {
        $uploadDir = wp_upload_dir();
        $cmacUploadDir = $uploadDir['basedir'] . '/' . ACC_UPLOAD_PATH;
    }
    if(!is_dir($cmacUploadDir)){	
        mkdir($cmacUploadDir);
    }
    return $cmacUploadDir;
}

/**
 * Returns the upload url
 * @return string
 */
function cmacc_get_upload_url() {
    static $cmacUpladDir = null;

    if (!$cmacUpladDir) {
        $uploadDir = wp_upload_dir();
        $cmacUploadDir = set_url_scheme($uploadDir['baseurl']) . '/' . ACC_UPLOAD_PATH;
    }
    return $cmacUploadDir;
}

/*
 * plugin activation
 */

function ads_cc_activate() {
    return true;
}

/**
 * Plugin deactivation
 */
function ads_cc_deactivate() {
    global $wpdb;
//	$wpdb->query('DROP TABLE IF EXISTS '.PENDING_EVENTS_TABLE);
}

/**
 * Function checks whether both the server and the campaign are active
 * @param type $campaignId
 * @return boolean
 */
function cm_acc_check_server_and_campaign($campaignId, $isGroup = FALSE) {
    global $CMAdChangerClientCampaignCache, $CMAdChangerClientGroupCache, $CMAdChangerClientError;

    if ($CMAdChangerClientCampaignCache === NULL) {
        cm_acc_get_campaigns_info();
    }

    if (empty($campaignId) || !is_numeric($campaignId)) {
        $CMAdChangerClientError = 'Wrong campaign ID';
        return FALSE;
    }

    if ($isGroup) {
        if (!empty($CMAdChangerClientGroupCache)) {
            foreach ($CMAdChangerClientGroupCache as $group) {
                if ($group['group_id'] == $campaignId) {
                    return $group['campaign_id'];
                }
            }
        }
    } else {
        if (!empty($CMAdChangerClientCampaignCache)) {
            foreach ($CMAdChangerClientCampaignCache as $campaign) {
                if ($campaign['campaign_id'] == $campaignId) {
                    /*
                     * If campaign found but not active
                     */
                    if (!$campaign['campaign_active']) {
                        return FALSE;
                    }
                    return $campaign['campaign_id'];
                }
            }
        }
    }
    if (empty($CMAdChangerClientError)) {
        $CMAdChangerClientError = 'No campaign with ID: ' . $campaignId . ' on the server!';
    }

    return FALSE;
}

add_action('wp_ajax_nopriv_acc_trigger_click_event', array('AC_Client', 'trigger_click_event'));
add_action('wp_ajax_acc_trigger_click_event', array('AC_Client', 'trigger_click_event'));
add_action('wp_ajax_nopriv_acc_get_banners', array('AC_Client', 'get_banners'));
add_action('wp_ajax_acc_get_banners', array('AC_Client', 'get_banners'));
add_action('wp_ajax_nopriv_acc_trigger_impression_event', array('AC_Client', 'trigger_impression_event'));
add_action('wp_ajax_acc_trigger_impression_event', array('AC_Client', 'trigger_impression_event'));



/*
 * Trigger the checks to ensure we need to load Custom CSS and JS
 */
add_filter('the_posts', 'cm_ads_enqueue_the_posts');
add_action('wp_enqueue_scripts', 'cm_ads_enqueue_init');

/**
 * Function outputs the CSS required for ads to display properly
 */
function cmac_output_css() {
    $content = '';
    $custom_css = get_option('acc_custom_css', '');

    if ($custom_css) {
        $content .= trim($custom_css);
        $content .= "\n<!--CMAC Custom CSS: END-->\n";
    }
    return trim($content);
}

/**
 * Function outputs the CSS and JS required for ads to display properly
 */
function cmac_enqueue_js_css() {
    $inFooter = get_option('acc_script_in_footer', false);
    wp_enqueue_script('tcycle', plugins_url('assets/js/jquery.tcycle.js', __FILE__), array('jquery'), '1.0.0', $inFooter);
    wp_enqueue_style('cmac_styles', plugins_url('assets/css/cmac.css', __FILE__));

    /*
     * It's WP 3.3+ function
     */
    if (function_exists('wp_add_inline_style')) {
        wp_add_inline_style('cmac_styles', cmac_output_css());
    }
    if (!defined('CMAC_HEAD_ENQUEUED')) {
        define('CMAC_HEAD_ENQUEUED', 1);
    }
}

/**
 * Function enqueues the JS and CSS if they're required
 *
 * @param type $posts
 * @return type
 */
function cm_ads_enqueue_the_posts($posts) {
    remove_filter('the_posts', 'cm_ads_enqueue_the_posts');
    apply_filters("debug", "Before cm_ads_enqueue_the_posts");

    /*
     * If client is deactivated
     */
    if (!cmacc_is_active()) {
        return $posts;
    }

    if (empty($posts)) {
        return $posts;
    }

    $shortcode_found = false; // use this flag to see if styles and scripts need to be enqueued

    foreach ($posts as $post) {
        if (has_shortcode($post->post_content, 'cm_ad_changer')) {
            $shortcode_found = TRUE;
            break;
        }
    }

    /*
     * If at least one of the conditions is TRUE - enqueue scripts and styles
     */
    if ($shortcode_found && !defined('CMAC_HEAD_ENQUEUED')) {
        cmac_enqueue_js_css();
    }

    apply_filters("debug", "After cm_ads_enqueue_the_posts");
    return $posts;
}

/**
 * Returns TRUE if client is active and FALSE otherwise
 * @return type
 */
function cmacc_is_active() {
    return get_option('acc_active', '0') != '0';
}

/**
 * Function enqueues the JS and CSS if they're required
 *
 * @param type $posts
 * @return type
 */
function cm_ads_enqueue_init() {
    /*
     * If client is deactivated
     */
    if (!cmacc_is_active()) {
        return;
    }

    $acc_inject_scripts = get_option('acc_inject_scripts', '0');
    $isWidgetDisplayed = CMACWidget::$widget_displayed;
    $isWidgetActive = is_active_widget(false, false, 'cmacwidget');
    $isGroupsWidgetActive = is_active_widget(false, false, 'cmacgwidget');
    /*
     * If at least one of the conditions is TRUE - enqueue scripts and styles
     */
    if (!defined('CMAC_HEAD_ENQUEUED') && ($acc_inject_scripts || $isWidgetDisplayed || $isWidgetActive || $isGroupsWidgetActive)) {
        cmac_enqueue_js_css();
    }
}

/**
 * Gather the informations about the campaigns running on the server once to limit the number of requests
 * @global type $CMAdChangerClientCampaignCache
 * @global type $CMAdChangerClientError
 */
function cm_acc_get_campaigns_info() {
    apply_filters("debug", "Before cm_acc_get_campaigns_info");
    global $CMAdChangerClientCampaignCache, $CMAdChangerClientGroupCache, $CMAdChangerClientError;

    $CMAdChangerClientError = '';

    $serverUrl = trailingslashit(get_option('acc_server_domain', ''));
    if (!empty($serverUrl)) {
        $responseString = AC_Client::curl_request($serverUrl . '?acs_action=get_campaigns_info');
        
        if (is_string($responseString)) {
            if(preg_match("/ERROR 403 - FORBIDDEN/", $responseString)){
                $CMAdChangerClientCampaignCache = array();
                $CMAdChangerClientError = 'This domain is forbibben to connect to the server. Please check server domain settings.';
            }
            $response = json_decode($responseString, TRUE);

            if (isset($response['success'])) {
                $CMAdChangerClientCampaignCache = $response['campaigns'];
                $CMAdChangerClientGroupCache = $response['groups'];
            } else {
                $CMAdChangerClientCampaignCache = array();
                $CMAdChangerClientError = isset($response['error_message']) ? $response['error_message'] : '';
            }
        } else {
            $CMAdChangerClientCampaignCache = array();
            $CMAdChangerClientError = 'Obtaining the Campaigns list from the server has failed: ' . $CMAdChangerClientError;
        }
    } else {
        $CMAdChangerClientCampaignCache = array();
        $CMAdChangerClientError = 'Server URL is not set';
    }
    apply_filters("debug", "After cm_acc_get_campaigns_info");
}

/**
 * Checks if the server is active
 *
 * @return type
 */
function cm_acc_check_server_activated() {
    return get_option('cm-ad-changer-server-active', '0') == '0';
}

if (!function_exists('parse_php_info')) {

    function parse_php_info() {
        ob_start();
        phpinfo(INFO_MODULES);
        $s = ob_get_contents();
        ob_end_clean();
        $s = strip_tags($s, '<h2><th><td>');
        $s = preg_replace('/<th[^>]*>([^<]+)<\/th>/', "<info>\\1</info>", $s);
        $s = preg_replace('/<td[^>]*>([^<]+)<\/td>/', "<info>\\1</info>", $s);
        $vTmp = preg_split('/(<h2>[^<]+<\/h2>)/', $s, -1, PREG_SPLIT_DELIM_CAPTURE);
        $vModules = array();
        for ($i = 1; $i < count($vTmp); $i++) {
            if (preg_match('/<h2>([^<]+)<\/h2>/', $vTmp[$i], $vMat)) {
                $vName = trim($vMat[1]);
                $vTmp2 = explode("\n", $vTmp[$i + 1]);
                foreach ($vTmp2 AS $vOne) {
                    $vPat = '<info>([^<]+)<\/info>';
                    $vPat3 = "/$vPat\s*$vPat\s*$vPat/";
                    $vPat2 = "/$vPat\s*$vPat/";
                    if (preg_match($vPat3, $vOne, $vMat)) { // 3cols
                        $vModules[$vName][trim($vMat[1])] = array(trim($vMat[2]), trim($vMat[3]));
                    } elseif (preg_match($vPat2, $vOne, $vMat)) { // 2cols
                        $vModules[$vName][trim($vMat[1])] = trim($vMat[2]);
                    }
                }
            }
        }
        return $vModules;
    }

}

if (!function_exists('cminds_units2bytes')) {

    function cminds_units2bytes($str) {
        $units = array('B', 'K', 'M', 'G', 'T');
        $unit = preg_replace('/[0-9]/', '', $str);
        $unitFactor = array_search(strtoupper($unit), $units);
        if ($unitFactor !== false) {
            return preg_replace('/[a-z]/i', '', $str) * pow(2, 10 * $unitFactor);
        }
    }

}

if (!function_exists('cmac_log')) {

    function cmac_log($message) {
        if (CMAC_DEBUG != '1') {
            return;
        }

        $f = fopen(ACS_PLUGIN_PATH . '/log.txt', 'a');
        fwrite($f, date('Y-m-d H:i:s') . ': ' . $message . "\n");
        fclose($f);
    }

}
if (!function_exists('p')) {
    function p($var, $exit = false, $silent = false){
        if($silent){
            echo "<!--";
        }
        echo "<pre>\n";
        print_r($var);
        echo "</pre>\n";
        if($silent){
            echo "-->";
        }
        if($exit){
            exit();
        }
    }
}
if (!function_exists('viewMessage')) {
    function viewMessage($message = '', $type = 'message'){
        switch ($type){
            case 'message' : echo '<span style="color: green;">'.$message.'</span>'; break;
            case 'warning' : echo '<span style="color: yellow;">'.$message.'</span>'; break;
            case 'error' : echo '<span style="color: red;">'.$message.'</span>'; break;
            default: break;
        }
    }
}