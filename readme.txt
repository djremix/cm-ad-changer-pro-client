=== Plugin Name ===
Name: CM Ad Changer - Client Pro
Contributors: CreativeMindsSolutions
Donate link: http://ad-changer.cminds.com/
Tags:ad,ad,ad network,adbrite,adroll,ads,ads manager,adsense,adserver,advertisement,advertising,ajax,banner,banner ad,banner changer,banner manager,banner rotator,banners,carousel,click,click counter,clicks,commission,geo location,geolocation,google,image,image rotate,Impression,Impressions,income,inject,injection,insert,junction,manager,media,monetise,monetize,money,notification,notifications,promotion,promotions,random,referral,report,reporting,reports,responsive,responsive banner,revenue,rotate,rotating,rotator,seo,server,slider,statistics,stats,track,tracking,variation,variations,ypn
Requires at least: 3.4
Tested up to: 4.0.0
Stable tag: 1.6.8

Manage, Track and Report the Advertising Campaigns and Banners on Your Site. Turn your WP into an Ad Server!

== Description ==

The CM Ad Changer can help you manage, track and provide reports of how your advertising campaigns are being run and turns your WordPress site into an ad server.

[Visit Plugin Site](http://ad-changer.cminds.com/)

[youtube http://www.youtube.com/watch?v=rBGl2ENV5Fc]

The CM Ad Changer client includes basic connection information to retrieve campaign intelligence from the ad changer server.
The ad changer server includes a campaign management panel and statistics modules that shows how a campaign performs.
By managing advertising campaigns across several Word Press sites at the same time, the CM ad changer makes controlling and overseeing online banner and image promotions easier than ever.

Each campaign can manage unlimited number of images and banners.
Utilize your banners within your site, by rotating them in random order or even have them targeted with unique URLs.
Measure conversion and count impressions of your banners, use short code to insert banners into posts and pages, while supporting debug mode.

**Use-Cases**

* Banners Management - Manage banners within your site.
* Rotation - Show rotating images/banners in random order anywhere in your site.
* Conversions - Measure banner conversion.
* Impressions - Count banner impressions.
* Ad Server - Build an ad server connected with several client simultaneously.

**Plugin Site**

* [Plugin Site](http://ad-changer.cminds.com/)

**Features**

* Includes impression &amp; clicks count per each banner [view image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/image-a1.png)
* Each campaign can serve banners randomly or selectively.
* Each banner can has it's own weight allowing it to have more impressions when selected randomly [view image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/image-a1.png)
* Each banner can have a unique target URL [view image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/image-a1.png)
* Ads are inserted into post / pages using shortcode.
* Shortcode support debug mode [view image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/image7.png)
* Shortcode also includes ability to wrap banner with div.
* Can run several campaigns simultaneously [view image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/image2.png)
* Each campaign can manage unlimited number of images / banners.

**Pro Version Features**

[Pro Version](http://ad-changer.cminds.com/)

The CM Ad Changer Pro version Includes all the free version features with the addition of the following features:

* Responsive Banners - support adaptive banner size based on browser screen size. Support mobile devices while service the correct banner size [View First Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac2.png), [View Second Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac6.png)
* Notifications - Campaign manager will receive notifications to his email when campaign stops [View Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac4.png)
* Rotating Banners - Support rotating banners [View Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac6.png)
* Client Plugin - Includes a client plugin which can be installed on a remote WP installation and serve campaign banners from the CM Ad Changer Server. Each server can serve many clients simultaneously [View Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac92.png)
* Statistics - Include several statistics and access log modules. Log can be downloaded in csv format. Statistics reports can be shown by month. Statistics also include geo location information by country name. [View first Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac8.png), [View second Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac9.png), [View third Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac91.png)
* Restrict by Dates - Campaign can be restricted based on dates [View Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac7.png)
* Restrict by Days - Campaign can be restricted based on days in the week [View Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac7.png)
* Restrict by Domains - Campaign can be restricted based on client domains (which Ad Changer clients will be served or not per a specific campaign) [View Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac5.png)
* Restrict by Clicks - Campaign can be restricted when it reaches a max amount of clicks [View Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac5.png)
* Restrict by Impressions - Campaign can be restricted when it reaches a max amount of Impressions [View Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac5.png)
* Custom JS - JS function can be added per campaign and executed once banner is clicked [View Image](http://ad-changer.cminds.com/wp-content/uploads/ad-changer/ac5.png)

**More Plugins by CreativeMinds**

* [CM Super ToolTip Glossary](http://wordpress.org/extend/plugins/enhanced-tooltipglossary/) - Easily creates a Glossary, Encyclopaedia or Dictionary of your website's terms and shows them as a tooltip in posts and pages when hovering. With many more powerful features.
* [CM Download Manager](http://wordpress.org/extend/plugins/cm-download-manager) - Allows users to upload, manage, track and support documents or files in a download directory listing database for others to contribute, use and comment upon.
* [CM Answers](http://wordpress.org/extend/plugins/cm-answers/) - Allows users to post questions and answers (Q&A) in a Stack-overflow style community forum which is easy to use, customize and install. Comes with Social integration Shortcodes.


== Installation ==

1. Upload the plugin folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Manage your CM Ad Changer plugin from Left Side Admin Dashboard

== Frequently Asked Questions ==

= How can I add banners ? =

Simply add a campaign and define upload banners. You can then insert a shortcode into theme or any post or page and banner will be show as defined in the campaign parameters .

== Screenshots ==

1. Banners management
2. Set dates when campaign is active
3. Set active days
4. Plugin settings
5. Campaigns manager



== Changelog ==
 1.6.8 =
* Fixed error notice about wp_enqueue_script being called incorrectly

 1.6.7 =
* Added new functionality to include custom js code for HTML banner type.

 1.6.6 =
* issue with adsense campaigns resolution fixed. From now the adsense container will adjust automatically to parent container

 1.6.5 =
* enque js and css scripts fix
* minor fixes with settings options not being stored in database
* added check if the nonce is correct during saving the settings

 1.6.4 =
* Removed old resend events functionality, due to database errors

 1.6.3 =
* Added new feature that allows to check connection between client and server.

 1.6.2 =
* Minor bug fixes

 1.6.1 =
* Fixed bug with campaign setting "Open target URL in new window" not being executed during banner click event
* Fixed bug with opening link in new window being blocked as popup in Chrome

 1.6.0 =
* Added security precautions for XSS attacks during add_query_arg function usage

 1.5.6 =
* Added new flat share box

 1.5.5 =
* Fixed error with upload directory when resizing images

 1.5.4 =
* Fixed redirect url bug with HTML and Video ads

 1.5.3 =
* Added fix to licensing API cached results that was causing activation problems for some users

 1.5.2 =
* Added option to disable sending history data (impressions and clicks) back to server
* Fixed errors and notices that caused error notification message during plugin activation

 1.4.6 =
* Fixed MISC issues with flying ad and flying bottom ad

 1.4.5 =
* Fixed issue with wordpress v4.01 new js libraries path

 1.4.4 =
* New feature. New banner type: floating bottom banner
* General bugfixing

 1.4.3 =
* Added new report, report by group id
* Fixed rotated banner link issue

= 1.4.2 =
* Added manual version check option

= 1.4.1 =
* Fixed the frequency the plguin checks for the update

= 1.4.0 =
* New feature. New banner type: floating banner

= 1.3.9 =
* Added the notice about the new version of the plugin available

= 1.3.6 =
* Fixed Firefox Issue with opening links after clicking the banner
* General bugfixing

= 1.3.5 =
* Fixed the conflict of Widget class
* Fixed the links not opening in the Firefox
* Improved the debug information on "Testing" tab

= 1.3.4 =
* Fixed the conflict of CSS classes

= 1.3.3 =
* Fixed the problems with "target_blank"
* Hidden the AdChanger admin menu for non-admin users
* Improved the detection on the inactive campaigns
* Added the admin notice about Client being inactive
* Updated the ID generation function to fix rare issues of ID clash
* Added the support for Width and Height of the HTML Campaigns

= 1.3.2 =
* Fixed the conflict between the CM Ad Changer widget and the Page Builder plugin

= 1.3.0 =
* Fixed bug with wrong uploads directory
* Changed the way testing works - now will alert if somethin's wrong
* General bugfixing
* Added the Call to action box on Settings screen

= 1.2.9 =
* Fixed some problems with HTML Ads
* Added the support for the Video Ads

= 1.2.8 =
* Improved the support for HTML Ads
* Added the "target_blank" (default 1) to the shortcode - now it's possible to open links in the same window
* Added the support for the Campaign Groups

= 1.2.7 =
* Added the support for HTML Ads
* Improved the support for AdSense Ads
* Added the possibility to setup the banners height & width
* Improved the performance

= 1.2.5 =
* Added the support for the multisite
* Fixed some performance issues
* Fixed the widget class
* Added the AdSense support
* Fixed some bugs

= 1.2.0 =
* General code refactoring and fixes

= 1.1.2 =
* Settings tabified
* Custom CSS class to widget added

= 1.1.0 =
* Replace get_bloginfo('url') with get_bloginfo('wpurl') for all relative paths

= 1.0.0 =
* Initial release

