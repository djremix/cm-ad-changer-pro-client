<?php

/**
 * CM Ad Changer
 *
 * @author CreativeMinds (http://ad-changer.cminds.com)
 * @copyright Copyright (c) 2013, CreativeMinds
 */
class AC_Client {

    public static $uniqueIds = array();

    public static function getRandomId() {
        $randomId = md5(uniqid()); // is required to distinguish banners which are on the same page

        while (in_array($randomId, self::$uniqueIds)) {
            $randomId = md5(uniqid());
        }

        self::$uniqueIds[] = $randomId;
        return $randomId;
    }

    /**
     * [cm_ad_changer] shortcode
     * @return String
     * @param Array   $args  Shortcode arguments
     */
    public static function banners($args) {
        global $CMAdChangerClientError;
        $historyDisabled = get_option('acs_disable_client_history_table', null);
        apply_filters("debug", "Before banners()");

        /*
         * If client is deactivated
         */
        if (!cmacc_is_active()) {
            return;
        }
        $groupId = null;
        $initialArgsArray = array(
            'campaign_id' => null,
            'group_id' => null,
            'linked_banner' => '1',
            'no_responsive' => '0',
            'demonstration_id' => null,
            'custom_css' => '',
            'container_width' => null,
            'class' => null,
            'debug' => FALSE,
            'height' => null,
            'width' => null,
            'allow_inject_html' => '1',
            'allow_inject_js' => '1',
            'wrapper' => '0',
        );
        /*
         * No default arg value
         */
        if (isset($args['target_blank']) && is_numeric($args['target_blank'])) {
            $initialArgsArray['target_blank'] = $args['target_blank'];
        }
        $args = shortcode_atts($initialArgsArray, $args);
        $groupId = $args['group_id'];
        $CMAC_HEAD_ENQUEUED = defined('CMAC_HEAD_ENQUEUED');
        $isGroup = FALSE;

        if (is_array($args)) {
            if (!empty($args['group_id'])) {
                $campaign_id = $args['group_id'];
                $isGroup = TRUE;
                $groupId = $args['group_id'];
                unset($args['group_id']);

                if (empty($campaign_id)) {
                    return 'No campaigns in group!';
                }
            } else {
                $campaign_id = $args['campaign_id'];
            }
        } elseif (is_numeric($args)) {
            $campaign_id = $args;
        } else {
            return 'Wrong campaign ID';
        }

        /*
         * At this moment we have to check if server and campaign are active
         */
        $serverAndCampaign = cm_acc_check_server_and_campaign($campaign_id, $isGroup);
        apply_filters("debug", "After check server and campaign");

        /*
         * Something went wrong e.g. server is inactive, or campaigng is inactive etc.
         */
        if (!$serverAndCampaign) {
            apply_filters("debug", "After banners() - error");
            return $CMAdChangerClientError;
        }

        /*
         * After the change with the groups it has to be done this way since we first have to get the campaign_id from the group
         * but we don't want to make a separate call just for this
         */
        $campaign_id = $serverAndCampaign;
        $args['campaign_id'] = $campaign_id;

        $demonstration_id = self::getRandomId();
        $banner_class = 'acc_banner_link' . $demonstration_id;
        $slideshow_id = 'acc_div' . $demonstration_id;
        $js_callback_function = 'anySliderCallback' . $demonstration_id;
        $globalVariationsSetting = get_option('acc_use_banner_variations', '1') == '1';
        $localVariationsSetting = !isset($args['no_responsive']) || isset($args['no_responsive']) && $args['no_responsive'] != '1';

        /*
         * Use variations only if enabled both globally and locally
         */
        $use_banner_variations = $globalVariationsSetting && $localVariationsSetting;

        $ret_html = '';

        if (!$use_banner_variations) {
            $args['demonstration_id'] = $demonstration_id;

            $requested_banners = self::request_banners($args, $groupId);

            if (isset($requested_banners['error'])) {
                apply_filters("debug", "After banners() - error");
                $returnVal = !empty($args['debug']) ? $requested_banners['html'] : '';
                return $returnVal;
            }
            $ret_html .= $requested_banners['html']; // if no banner variations required => request the banners directly

            $ret_html .= '<script type="text/javascript">
				jQuery(document).ready(function(){
					cm_bind_click_banner' . $demonstration_id . '();
					' . ($CMAC_HEAD_ENQUEUED ? 'cm_init_slider' . $demonstration_id . '();' : '') . '
				});
                         </script>';
        } else {
            $banner_area = get_option('acc_banner_area', 'screen');
            $ret_html = '<div id="' . $slideshow_id . '"></div>';

            $ret_html .= '<script type="text/javascript">';

            /*
             *  Create AJAX request in case if banner variations are allowed
             */
            if ($banner_area == 'container') {
                $ret_html .='
				width = jQuery("#' . $slideshow_id . '").parent().width();
				if(parseInt(width)<=0){
					width = jQuery(window).width();}';
            } elseif ($banner_area == 'screen') {
                $ret_html .= 'width = jQuery(window).width();';
            }
            $ret_html .='
                jQuery.ajax({
                        url: "' . get_bloginfo('wpurl') . '/wp-admin/admin-ajax.php?action=acc_get_banners",
                        type: "post",
                dataType: "json",
                        data:{
                                args: "' . addslashes(serialize($args)) . '",
                                container_width: width,
                                demonstration_id: "' . $demonstration_id . '",
                                group_id: ' . intval($groupId) . '
                        }
                }).done(function(response){
                if(typeof(response.error) !== "undefined")
                {
                    if(typeof(response.html) !== "undefined")
                    {
                        alert(response.html);
                    }
                    return;
                }
                jQuery("#' . $slideshow_id . '").after(response.html);
                        jQuery("#' . $slideshow_id . '").remove();
                        cm_bind_click_banner' . $demonstration_id . '();
                        cm_init_slider' . $demonstration_id . '();
                })';
            $ret_html .= '</script>';
        }

        /*
         * Custom CSS
         */
        if (!empty($args['custom_css'])) {
            $ret_html .= '<style type="text/css">' . trim($args['custom_css']) . '</style>';
        }

        $ret_html .= '<script type="text/javascript">';

        $ret_html .= '
			function cm_bind_click_banner' . $demonstration_id . '(){
                                var currentEl = jQuery(".' . 'acc_banner_link' . $demonstration_id . '");
				jQuery(currentEl).on("click",function(e){
                                meBannerObject = this;
                                if (meBannerObject.target == "_blank"){
                                    var newWindowObject = window.open("", meBannerObject.target);
                                }';
        if ($historyDisabled != 1) {
            $ret_html .= 'jQuery.ajax({url: "' . get_bloginfo('wpurl') . '/wp-admin/admin-ajax.php?action=acc_trigger_click_event",
                                type: "post",
                                async: false,
                                data: {campaign_id: ' . $campaign_id . ', banner_id: jQuery(this).attr("banner_id")' . ((!empty($groupId)) ? (", group_id: " . $groupId) : ("")) . '},
                                complete:
                                  function(){
                                      if(!meBannerObject.href){
                                          return false;
                                      }
                                      if (meBannerObject.target == "_blank"){ 
                                          newWindowObject.location = meBannerObject.href;
                                      }else { 
                                          document.location = meBannerObject.href; 
                                      }
                                  }
                            });';
        } else {
            $ret_html .= '
                                    if(!meBannerObject.href){
                                        return false;
                                    }
                                    if (meBannerObject.target == "_blank"){ 
                                        newWindowObject.location = meBannerObject.href; 
                                    }else { 
                                        document.location = meBannerObject.href; 
                                    }';
        }
        $ret_html .= '
                                        e.preventDefault();
                                        return false;
				});
			}

			function cm_init_slider' . $demonstration_id . '(){
				jQuery("#' . $slideshow_id . '").tcycle();
			}';

        if ($historyDisabled != 1) {
            $ret_html .= 'function ' . $js_callback_function . '(current_banner_index){
                            jQuery.ajax({url: "' . get_bloginfo('wpurl') . '/wp-admin/admin-ajax.php?action=acc_trigger_impression_event",
                                            type: "post",
                                            data: {
                                                campaign_id: ' . $campaign_id . ',
                                                banner_id: jQuery(".' . $banner_class . '").eq(current_banner_index-1).attr("banner_id")' . ((!empty($groupId)) ? (", group_id: " . $groupId) : ("")) . '
                                            }
                                        });
			}';
        }
        $ret_html .= '</script>';

        apply_filters("debug", "After banners()");

        return $ret_html;
    }

    /**
     * Banner output
     * @return String
     * @param Array   $args  Shortcode arguments
     */
    public static function request_banners($args, $groupId = null) {
        apply_filters("debug", "Before AC_Client::request_banners()");
        cmac_log('AC_Client::request_banners()');

        if (is_array($args)) {
            $campaign_id = $args['campaign_id'];
        } elseif (is_numeric($args)) {
            $campaign_id = $args;
        } else {
            return 'Wrong campaign ID';
        }

        $server_url = get_option('acc_server_domain');
        $url = $server_url . '/?acs_action=get_banner&campaign_id=' . $campaign_id . '&group_id=' . $groupId;
        if (isset($args['container_width'])) {
            $url .= '&container_width=' . $args['container_width'];
        }
        $data = self::curl_request($url, 'http_referer=' . get_bloginfo('wpurl'));

        $ret_html = '';

        $response = json_decode($data, true);

        if (isset($args['debug']) && $args['debug'] == '1') {
            if (is_array($response)) {
                $ret_html = self::cmac_format_list($response, 'CM AdChanger Debug Info:', 'acc_debug');
            }
        }

        if (!isset($response['error'])) {
            if (isset($args['class']) && !empty($args['class'])) {
                $css_class = $args['class'];
            } else {
                $css_class = null;
            }

            if (isset($args['wrapper']) && $args['wrapper'] == '1') {
                $ret_html .= '<div' . (!is_null($css_class) ? ' class="' . $css_class . '"' : '') . '>';
                $css_class = null; // not needed in other tags
            }

            if (!empty($args['allow_inject_js']) && !empty($response['custom_js'])) {
                $ret_html .= '<script type="text/javascript">';
                $ret_html .= stripslashes($response['custom_js']);
                $ret_html .= '</script>';
            }
            switch ($response['campaign_type_id']) {
                case '0':
                    $ret_html .= self::displayImageBanner($response, $ret_html, $args);
                    break;
                case '1':
                    if (!empty($args['allow_inject_html'])) {
                        $ret_html .= self::displayHTMLBanner($response, $ret_html, $args);
                    }
                    break;
                case '2':
                    $ret_html .= self::displayAdsenseBanner($response, $ret_html, $args);
                    break;
                case '3':
                    if (!empty($args['allow_inject_html'])) {
                        $ret_html .= self::displayVideoBanner($response, $ret_html, $args);
                    }
                    break;
                case '4':
                    $ret_html = self::displayFloatingBanner($response, $ret_html, $args);
                    break;
                case '5':
                    $ret_html = self::displayFloatingBottomBanner($response, $ret_html, $args);
                    break;
                default:
                    break;
            }

            /*
              if(get_option('acc_div_wrapper','0')=='1')
              $ret_html .= '</div>';
             */
            if (isset($args['wrapper']) && $args['wrapper'] == '1') {
                $ret_html .= '</div>';
            }

            apply_filters("debug", "After AC_Client::request_banners()");

            $retArr = array(
                'html' => $ret_html,
            );
            return $retArr;
        } else {
            cmac_log('Error response from the server: ' . $response['error']);
            apply_filters("debug", "After AC_Client::request_banners()");

            if (isset($args['debug']) && $args['debug'] == '1') {
                $retArr = array(
                    'html' => 'CM AdChanger Banner Request Error: "' . $response['error'] . '"',
                );
            }

            $retArr['error'] = 1;
            return $retArr;
        }
    }

    public static function displayImageBanner($response, $ret_html, $args) {
        $demonstration_id = $args['demonstration_id'];
        $use_banner_variations = get_option('acs_use_banner_variations', '1') == '1';
        $resize_banner = get_option('acs_resize_banner', '1') == '1';

        $tcycle_fx = get_option('acs_slideshow_effect', 'fade') == "fade" ? "fade" : "scroll";
        $tcycle_speed = get_option('acs_slideshow_transition_time', '400');
        $tcycle_timeout = get_option('acs_slideshow_interval', '5000');

        $banner_class = 'acc_banner_link' . $demonstration_id;

        $target = '';
        if (isset($args['target_blank'])) {
            if ($args['target_blank'] == 1) {
                $target = 'target="_blank"';
            }
        } elseif (isset($response['banner_new_window'])) {
            if ($response['banner_new_window'] == 1) {
                $target = 'target="_blank"';
            }
        }
        $css_class = ((!isset($args['wrapper']) || !$args['wrapper']) && !is_null($args['class']) ? $args['class'] : '');

        if (!isset($response['banners'])) {
            /*
             * if one banner was received
             */
            cmac_log('1 banner received');

            if ($use_banner_variations && $resize_banner && isset($response['resize']) && isset($args['container_width'])) {
                $image_content = file_get_contents($response['image']);
                $info = pathinfo($response['image']);
                $new_filename = cmacc_get_upload_dir() . $info['filename'] . '.' . $info['extension'];
                $thumb_filename = cmacc_get_upload_dir() . $info['filename'] . '_w' . $args['container_width'] . '.' . $info['extension'];
                $thumb_url = cmacc_get_upload_url() . $info['filename'] . '_w' . $args['container_width'] . '.' . $info['extension'];
                $f = fopen($new_filename, 'w');
                fwrite($f, $image_content);
                fclose($f);

                $image = new Image($new_filename);
                $image->resize($args['container_width']);
                $image->save($thumb_filename);
            }
            $img_html = '';
            $alt = (isset($response['alt_tag']) && !empty($response['alt_tag'])) ? ' alt="' . $response['alt_tag'] . '"' : '';
            $title = (isset($response['title_tag']) && !empty($response['title_tag'])) ? ' title="' . $response['title_tag'] . '"' : '';

            if (isset($response['banner_link']) && ($args['linked_banner'] == '1' || !isset($args['linked_banner']))) {
                $img_html .= '<img src="' . (isset($thumb_url) ? $thumb_url : $response['image']) . '"' . $alt . $title . ' />';
                $ret_html .= '<a href="' . $response['banner_link'] . '" ' . $target . ' banner_id="' . $response['banner_id'] . '" class="' . $banner_class . ' ' . (!is_null($css_class) ? $css_class : '') . '">' . $img_html . '</a>';
            } else {
                $img_html .= '<img src="' . (isset($thumb_url) ? $thumb_url : $response['image']) . '"' . $alt . $title . '' . (!is_null($css_class) ? ' class="' . $css_class . '"' : '') . ' />';
                $ret_html .= $img_html;
            }
        } else {
            /*
             *  several banners received
             */
            cmac_log('Several banners received');

            $width = isset($args['width']) ? $args['width'] : 'auto';
            $max_height = 0;

            if ($width != 'auto') {
                // finding max banner height, to define slideshow height
                $heights = array();
                foreach ($response['banners'] as $banner) {
                    $heights[] = $banner['image_height'];
                }
                $max_height = max($heights);
            }

            $height = isset($args['height']) ? 'height="' . $args['height'] . '"' : ($max_height) ? 'height="' . $max_height . '"' : '';

            $slideshow_id = 'acc_div' . $demonstration_id;
            $ret_html .= '<div id="' . $slideshow_id . '" style="text-align: center;width: ' . $width . ';' . $height . '" data-fx="' . $tcycle_fx . '" data-speed="' . $tcycle_speed . '" data-timeout="' . $tcycle_timeout . '">' . "\n";

            $bannerDivDisplay = 'style="display:block"';

            foreach ($response['banners'] as $banner) {
                $thumb_url = null;
                if ($use_banner_variations && $resize_banner && isset($banner['resize']) && isset($args['container_width'])) {
                    $image_content = file_get_contents($banner['image']);
                    $info = pathinfo($banner['image']);
                    $new_filename = cmacc_get_upload_dir() . $info['filename'] . '.' . $info['extension'];
                    $thumb_filename = cmacc_get_upload_dir() . $info['filename'] . '_w' . $args['container_width'] . '.' . $info['extension'];
                    $thumb_url = cmacc_get_upload_url() . $info['filename'] . '_w' . $args['container_width'] . '.' . $info['extension'];
                    $f = fopen($new_filename, 'w');
                    fwrite($f, $image_content);
                    fclose($f);

                    $image = new Image($new_filename);
                    $image->resize($args['container_width']);
                    $image->save($thumb_filename);
                }

                $ret_html .= '<div ' . $bannerDivDisplay . '>';
                /**
                 * In case something's wrong with JS show only the first banner
                 */
                if ($bannerDivDisplay == 'style="display:block"') {
                    $bannerDivDisplay = 'style="display:none"';
                }

                $img_html = '';
                $alt = (isset($banner['alt_tag']) && !empty($banner['alt_tag'])) ? ' alt="' . $banner['alt_tag'] . '"' : '';
                $title = (isset($banner['title_tag']) && !empty($banner['title_tag'])) ? ' title="' . $banner['title_tag'] . '"' : '';

                if (isset($banner['link']) && ($args['linked_banner'] == '1' || !isset($args['linked_banner']))) {
                    $img_html .= '<img src="' . (!is_null($thumb_url) ? $thumb_url : $banner['image']) . '"' . $alt . $title . ' />';
                    $ret_html .= '<a href="' . $banner['link'] . '" ' . $target . ' banner_id="' . $banner['id'] . '" class="' . $banner_class . ' ' . (!is_null($css_class) ? $css_class : '') . '">' . $img_html . '</a>';
                } else {
                    $img_html .= '<img src="' . (!is_null($thumb_url) ? $thumb_url : $banner['image']) . '"' . $alt . $title . '' . (!is_null($css_class) ? ' class="' . $css_class . '"' : '') . ' />';
                    $ret_html .= $img_html;
                }

                $ret_html .= '</div>' . "\n";
            }
            $ret_html .= '</div>';
        }

        return $ret_html;
    }

    public static function displayAdsenseBanner($response, $ret_html, $args) {
        static $adsenseInjected = false;

        /*
         * If there's AdSense - ignore the banners
         */
        if (!empty($response['adsense_client']) && !empty($response['adsense_slot'])) {
            if (!$adsenseInjected) {
                $ret_html .= '<script type="text/javascript" async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>';
            }

            $ret_html .= '<ins class="adsbygoogle"
     style="display:block;min-width: 300px; min-height: 250px;"
     data-ad-client="' . $response['adsense_client'] . '"
     data-ad-slot="' . $response['adsense_slot'] . '"></ins>';

            if (!$adsenseInjected) {
                $ret_html .= '<script type="text/javascript"> (adsbygoogle = window.adsbygoogle || []).push({}); </script>';
                $adsenseInjected = TRUE;
            }
        }

        return $ret_html;
    }

    public static function displayHTMLBanner($response, $ret_html, $args) {
        if (!empty($response['html']) || !empty($response['banner_custom_js'])) {
            $css_class = ((!isset($args['wrapper']) || !$args['wrapper']) && !is_null($args['class']) ? $args['class'] : '');
            $banner_class = 'acc_banner_link' . $args['demonstration_id'];
            $link = isset($response['link']) ? $response['link'] : null;
            $bannerId = isset($response['banner_id']) ? $response['banner_id'] : '';
            $target = '';
            if (isset($args['target_blank'])) {
                if ($args['target_blank'] == 1) {
                    $target = 'target="_blank"';
                }
            } elseif (isset($response['banner_new_window'])) {
                if ($response['banner_new_window'] == 1) {
                    $target = 'target="_blank"';
                }
            }

            $additionalStyle = '';
            if (!empty($response['width'])) {
                $additionalStyle .= 'max-width:' . $response['width'] . ';';
            }
            if (!empty($response['height'])) {
                $additionalStyle .= 'max-height:' . $response['height'] . ';';
            }

            if (!empty($additionalStyle)) {
                $banner_class .= ' cmac_limited_size';
            }
            if(!empty($response['banner_custom_js'])){
                $ret_html .= stripslashes($response['banner_custom_js']);
            }
            if(!empty($response['html'])){
                if (($args['linked_banner'] == '1' || !isset($args['linked_banner'])) && !empty($link)) {
                    $html_banner = '<div style="cursor:pointer;' . $additionalStyle . '" onclick="window.location=\'' . $link . '\'; return false;" ' . $target . ' banner_id="' . $bannerId . '" class="' . $banner_class . ' ' . (!is_null($css_class) ? $css_class : '') . '">' . $response['html'] . '</div>';
                } else {
                    $html_banner = $response['html'];
                }
            }
            $ret_html .= $html_banner;
        }


        return $ret_html;
    }

    public static function displayVideoBanner($response, $ret_html, $args) {
        if (!empty($response['html'])) {
            $css_class = ((!isset($args['wrapper']) || !$args['wrapper']) && !is_null($args['class']) ? $args['class'] : '');
            $banner_class = 'acc_banner_link' . $args['demonstration_id'];
            $link = isset($response['link']) ? $response['link'] : null;
            $bannerId = isset($response['banner_id']) ? $response['banner_id'] : '';
            $target = '';
            if (isset($args['target_blank'])) {
                if ($args['target_blank'] == 1) {
                    $target = 'target="_blank"';
                }
            } elseif (isset($response['banner_new_window'])) {
                if ($response['banner_new_window'] == 1) {
                    $target = 'target="_blank"';
                }
            }

            if (($args['linked_banner'] == '1' || !isset($args['linked_banner'])) && !empty($link)) {
                $html_banner = '<div style="cursor:pointer" onclick="window.location=\'' . $link . '\'; return false;" ' . $target . ' banner_id="' . $bannerId . '" class="' . $banner_class . ' ' . (!is_null($css_class) ? $css_class : '') . '">' . $response['html'] . '</div>';
            } else {
                $html_banner = $response['html'];
            }
            $ret_html .= $html_banner;
        }

        return $ret_html;
    }

    public static function displayFloatingBanner($response, $ret_html, $args) {
        /*
         * banner config resolve
         */
        $width = ((!empty($response['width']) ? ($response['width']) : ('600px')));
        $height = ((!empty($response['height']) ? ($response['height']) : ('400px')));
        $content = ((!empty($response['html']) ? ($response['html']) : ('')));
        $background = ((!empty($response['background']) ? ($response['background']) : ('#f0f1f2')));
        $userShowMethod = ((!empty($response['user_show_method']) ? ($response['user_show_method']) : ('always')));
        $underlayType = ((!empty($response['underlay_type']) ? ($response['underlay_type']) : ('dark')));
        $resetTime = ((!empty($response['reset_floating_banner_cookie_time']) ? ($response['reset_floating_banner_cookie_time']) : (7)));

        switch ($underlayType) {
            case 'dark' : $underlayColor = 'rgba(0,0,0,0.5)';
                break;
            case 'light' : $underlayColor = 'rgba(0,0,0,0.2)';
                break;
            default : $underlayColor = 'rgba(0,0,0,0.5)';
                break;
        }
        if (!empty($response['banner_edges'])) {
            switch ($response['banner_edges']) {
                case 'rounded' : $banner_edges = '4px';
                    break;
                case 'sharp' : $banner_edges = '0px';
                    break;
                default : $banner_edges = '4px';
            }
        } else {
            $banner_edges = '4px';
        }

        if (!empty($response['show_effect'])) {
            switch ($response['show_effect']) {
                case 'popin' : $show_effect = 'popin 1.0s';
                    break;
                case 'bounce' : $show_effect = 'bounce 1.0s';
                    break;
                case 'shake' : $show_effect = 'shake 1.0s';
                    break;
                case 'flash' : $show_effect = 'flash 0.5s';
                    break;
                case 'tada' : $show_effect = 'tada 1.5s';
                    break;
                case 'swing' : $show_effect = 'swing 1.0s';
                    break;
                case 'rotateIn' : $show_effect = 'rotateIn 1.0s';
                    break;
                default : $show_effect = 'popin 1.0s';
            }
        } else {
            $show_effect = 'popin 1.0s;';
        }
        $content = preg_replace("/'/", "\"", $content);
        $ret_html .= '<style>'
                . '#ouibounce-modal .modal {
                    width: ' . $width . ';
                    height: ' . $height . ';
                    background-color: ' . $background . ';
                    z-index: 1;
                    position: absolute;
                    margin: auto;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    border-radius: ' . $banner_edges . ';
                    -webkit-animation: ' . $show_effect . ';
                    animation: ' . $show_effect . ';
                  }'
                . (($underlayType != 'no') ? ('#ouibounce-modal .underlay {background-color: ' . $underlayColor . ';}') : ("")) .
                '</style>';
        $ret_html .= '<div id="ouibounce-modal">
            ' . (($underlayType != 'no') ? ('<div class="underlay"></div>') : ("")) . '
            <div class="modal">
            <div id="close_button"></div>
              <div class="modal-body">' . $content . '</div>
            </div>
          </div>';
        $ret_html .= "<script type='text/javascript'>
        jQuery(document).ready(function () {
            " . (($userShowMethod == 'always') ? ("setCookie('ouibounceBannerShown', 'true', -1);") : ("")) . "
            if(getCookie('ouibounceBannerShown') == ''){
                ouibounce = ouibounce(document.getElementById('ouibounce-modal'), {});
                setTimeout(function(){
                    ouibounce.fire();
                    " . (($userShowMethod == 'once') ? ("setCookie('ouibounceBannerShown', 'true', " . $resetTime . ");") : ("")) . "
                    }, 
                " . (((!empty($response['seconds_to_show']) && (intval($response['seconds_to_show']) > 0))) ? (intval($response['seconds_to_show']) * 1000) : ('0')) . ");
                jQuery('body').on('click', function() {
                  ouibounce.close();
                });

                jQuery('#ouibounce-modal #close_button').on('click', function() {
                  ouibounce.close();
                });

                jQuery('#ouibounce-modal .modal').on('click', function(e) {
                  e.stopPropagation();
                });
            }
        });";
        return $ret_html;
    }

    public static function displayFloatingBottomBanner($response, $ret_html, $args) {
        /*
         * banner config resolve
         */
        $width = ((!empty($response['width']) ? ($response['width']) : ('200px')));
        $height = ((!empty($response['height']) ? ($response['height']) : ('300px')));
        $content = ((!empty($response['html']) ? (preg_replace("/\"/", "'", $response['html'])) : ('')));
        $background = ((!empty($response['background']) ? ($response['background']) : ('#f0f1f2')));
        $userShowMethod = ((!empty($response['user_show_method']) ? ($response['user_show_method']) : ('always')));
        $resetTime = ((!empty($response['reset_floating_banner_cookie_time']) ? ($response['reset_floating_banner_cookie_time']) : (7)));

        if (!empty($response['banner_edges'])) {
            switch ($response['banner_edges']) {
                case 'rounded' : $banner_edges = '10px';
                    break;
                case 'sharp' : $banner_edges = '0px';
                    break;
                default : $banner_edges = '10px';
            }
        } else {
            $banner_edges = '10px';
        }

        if (!empty($response['show_effect'])) {
            switch ($response['show_effect']) {
                case 'popin' : $show_effect = 'popin 1.0s';
                    break;
                case 'bounce' : $show_effect = 'bounce 1.0s';
                    break;
                case 'shake' : $show_effect = 'shake 1.0s';
                    break;
                case 'flash' : $show_effect = 'flash 0.5s';
                    break;
                case 'tada' : $show_effect = 'tada 1.5s';
                    break;
                case 'swing' : $show_effect = 'swing 1.0s';
                    break;
                case 'rotateIn' : $show_effect = 'rotateIn 1.0s';
                    break;
                default : $show_effect = 'popin 1.0s';
            }
        } else {
            $show_effect = 'popin 1.0s;';
        }
        $ret_html .= '
                <style>
                #flyingBottomAd {
                padding: 0px;
                z-index: 100;
                border-radius: ' . $banner_edges . ' 0 0;
                -moz-border-radius: ' . $banner_edges . ' 0 0;
                -webkit-border-radius: ' . $banner_edges . ' 0 0;
                background: ' . $background . ';
                box-shadow: 0 0 20px rgba(0,0,0,.2);
                width: ' . $width . ';
                height: ' . $height . ';
                position: fixed;
                bottom: 0;
                right: 0;
                -webkit-backface-visibility: visible!important;
                -ms-backface-visibility: visible!important;
                backface-visibility: visible!important;
                -webkit-animation: ' . $show_effect . ';
                -moz-animation: ' . $show_effect . ';
                -o-animation: ' . $show_effect . ';
                animation: ' . $show_effect . ';
                -webkit-transition: bottom .5s ease,background-position .5s ease;
                transition: bottom .5s ease,background-position .5s ease;
            }
            </style>
            ';
        $ret_html .= "<script type='text/javascript'>
                " . (($userShowMethod == 'always') ? ("setCookie('ouibounceBannerBottomShown', 'true', -1);") : ("")) . "
                if(getCookie('ouibounceBannerBottomShown') == ''){
                var _flyingBottomOui = flyingBottomAd({
                    htmlContent: '<div id=\"flyingBottomAd\"><span class=\"flyingBottomAdClose\"></span>" . addslashes($content) . "</div>',
                    delay: " . (((!empty($response['seconds_to_show']) && (intval($response['seconds_to_show']) > 0))) ? (intval($response['seconds_to_show']) * 1000) : ('0')) . "
                });
                " . (($userShowMethod == 'once') ? ("setCookie('ouibounceBannerBottomShown', 'true', " . $resetTime . ");") : ("")) . "
                }
                </script>
        ";
        return $ret_html;
    }

    /**
     * AJAX call - Gets banners from the server
     */
    public static function get_banners() {
        cmac_log('AC_Client::get_banners() - AJAX request');
        if (!isset($_REQUEST['args'])) {
            die('args not set');
        }

        if (!isset($_REQUEST['container_width'])) {
            $_REQUEST['container_width'] = 0;
        }

        if (!isset($_REQUEST['demonstration_id'])) {
            die();
        }

        $args = unserialize(stripslashes($_REQUEST['args']));
        $args['container_width'] = $_REQUEST['container_width'];
        $args['demonstration_id'] = $_REQUEST['demonstration_id'];
        $groupId = null;
        if (!empty($_REQUEST['group_id'])) {
            $groupId = intval($_REQUEST['group_id']);
        }
        $result = self::request_banners($args, $groupId);
        wp_send_json($result);
        exit;
    }

    /**
     * AJAX call  - Send trigger click request to the server
     */
    public static function trigger_click_event() {
        cmac_log('AC_Client::trigger_click_event()');
        $historyDisabled = get_option('acs_disable_client_history_table', null);
        if ($historyDisabled == 1) {
            cmac_log('AC_Client::trigger_click_event() disabled due to setting');
            exit;
        }
        if (!isset($_GET['server_url'])) {
            $server_url = get_option('acc_server_domain');
        } else {
            $server_url = $_GET['server_url'];
        }
        $groupId = null;
        if (!empty($_REQUEST['group_id'])) {
            $groupId = intval($_REQUEST['group_id']);
        }
        $timestamp = time();
        $url = $server_url . '/?acs_action=trigger_click_event&campaign_id=' . $_REQUEST['campaign_id'] . '&banner_id=' . $_REQUEST['banner_id'] . '&group_id=' . $groupId;
        $data = self::curl_request($url, 'http_referer=' . get_bloginfo('wpurl'));

        $data = json_decode($data, true);
        
        exit;
    }

    /**
     * AJAX call - Send trigger impression request to the server
     */
    public static function trigger_impression_event() {
        cmac_log('AC_Client::trigger_impression_event()');

        if (!isset($_GET['server_url'])) {
            $server_url = get_option('acc_server_domain');
        } else {
            $server_url = $_GET['server_url'];
        }

        $timestamp = time();
        $groupId = null;
        if (!empty($_REQUEST['group_id'])) {
            $groupId = intval($_REQUEST['group_id']);
        }
        $url = $server_url . '/?acs_action=trigger_impression_event&campaign_id=' . $_REQUEST['campaign_id'] . '&banner_id=' . $_REQUEST['banner_id'] . '&group_id=' . $groupId;
        ;
        $data = self::curl_request($url, 'http_referer=' . get_bloginfo('wpurl'));

        $data = json_decode($data, true);

        exit;
    }

    /**
     * CURL request
     * @param String   $url remote URL
     * @param String   $post POST data
     */
    public static function curl_request($url = null, $post = '') {
        global $CMAdChangerClientError;

        $url_parts = explode('?', $url);
        $curPage = isset($_SERVER["HTTP_REFERER"]) ? urlencode($_SERVER["HTTP_REFERER"]) : get_bloginfo('wpurl');
        if (count($url_parts) > 1) {
            $url .= '&user_ip=' . $_SERVER['REMOTE_ADDR'] . '&webpage_url=' . $curPage;
        } else {
            $url .= '?user_ip=' . $_SERVER['REMOTE_ADDR'] . '&webpage_url=' . $curPage;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, get_bloginfo('wpurl'));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $data = curl_exec($ch);

        $curlError = curl_error($ch);

        if (!empty($curlError) || empty($data)) {
            $CMAdChangerClientError = 'cURL Error: ' . curl_errno($ch) . ' Message:' . $curlError;

//            /*
//             * Fallback to file get contents
//             */
//            $data = file_get_contents($url);
//            if(!empty($data))
//            {
//                $CMAdChangerClientError = '';
//            }
        }
        curl_close($ch);

        return $data;
    }
    /**
     * Converter of array to html list
     * @return String
     * @param Array   $data  Array of strings
     * @param String  $title Title of list
     * @param String   $class  CSS class of list
     */
    public static function cmac_format_list($data, $title = '', $class = '') {
        $ret_html = '';

        if (!is_array($data)) {
            $ret_html .= 'Data format is wrong';
            return $ret_html;
        }
        if (!empty($title)) {
            $ret_html = '<strong>' . $title . '</strong><br/>';
        }
        $ret_html .= '<ul ' . (!empty($class) ? 'class="' . $class . '"' : '') . ' >';
        foreach ($data as $field => $value) {
            $ret_html .= '<li> <strong>' . $field . ':</strong> <pre>' . (!empty($value) ? var_export($value, true) : '- empty -') . '</pre></li>';
        }
        $ret_html .= '</ul>';
        return $ret_html;
    }

}
